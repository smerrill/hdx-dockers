user = 'www-data'
group = 'www-data'
bind = '0.0.0.0:9223'
workers = 2
accesslog = '/var/log/dataproxy.access.log'
errorlog = '/var/log/dataproxy.error.log'
loglevel = 'debug'

